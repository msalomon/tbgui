# from terminal: sudo python3 TBpythonGUI.py
# GUI code for (partially) controlling the LHCb ECAL UII test beam bench
# currently implemented: - stepper motors to control the orientation of the module(x2), 
#                        - LED driver for calibration purposed (8 channels)
#                        - attenuators (2 channels), through an arduino
# Developed by Matteo Salomoni
# TODO
# add error in case only one driver is found
# add error in case value (given) is NULL

import tkinter as tk
import time
from zaber_motion import Library
from zaber_motion.ascii import Connection
from zaber_motion.ascii import AlertEvent
from zaber_motion.ascii import AllAxes
from zaber_motion import Units, Library
import importlib
import pyvisa
from PyMata.pymata import PyMata
import numpy as np
from os.path import exists

def LED_driver_write(driverNum, channel, parameter, value):
# driverNum should start from 0
# parameters that we will have to access and modify in the drivers (in paranteses values example)
# to be accessed with inst.write(''). PULSE[n] refer to each channel. PULSE0 refers to the global device (e.g. Run/Stop button)
# :PULSE1:STATE (ON/OFF) 
# :PULSE1:WIDTH (0.00000002000)
# :PULSE1:OUTPUT:AMPLITUDE (9.6)
#   
    dict_channel = {
        "main"  : ":PULSE0",
        "ch1"   : ":PULSE1",
        "ch2"   : ":PULSE2",
        "ch3"   : ":PULSE3",
        "ch4"   : ":PULSE4"
    }
    
    dict_param= {
        "status": ":STATE",
        "amp"   : ":OUTPUT:AMPLITUDE",
        "width" : ":WIDTH"
    }
    if LED:
        command_string = (dict_channel[channel]+dict_param[parameter]+' '+str(value))
        print(command_string)
        #print(driver_list,driverNum)
        driver_list[driverNum].write(command_string)
        #check is the command was well set. We have to reset the connection here due to a firmware bug that prevent to query after writing.
        #reset
        time.sleep(0.1)
        rm = pyvisa.ResourceManager()
        driver_list[driverNum]=rm.open_resource('ASRL/dev/LEDdriver%d::INSTR' % driverNum)
        driver_list[driverNum].baud_rate = 115200
        #query the value set in the parameter
        query_string = (dict_channel[channel]+dict_param[parameter]+'?')
        answer = driver_list[driverNum].query(query_string)
        print(answer)
        #check if the command value is the same as the query answer
        value_set = float(answer.split()[0])
        print('value (given): ', value)
        print('value_set: ', value_set)
        if value_set != float(value):
            print('error! value was not set properly')
            return 0
        else:
            print('parameter',dict_param[parameter],'was correctly set to ',value,' for channel ',channel, ' of driver ', driverNum)
            return 1
    else:
        print("Warning: LED flag not active")

# setting default values from lab measurements of the SPACAL-W GAGG
def spacal_lab_default(): 
    # initializing the vectors
    if var_module.get()=="Wgagg":
        amplitude_init_driver0 = [9.5, 9.5, 8.3, 8.3]
        width_init_driver0 = [10,10,10,10]
        amplitude_init_driver1 = [9.1, 9.4, 8.3, 8.3]
        width_init_driver1 = [10,10,10,10]   
    elif var_module.get()=="PbPoly":
        amplitude_init_driver0 = [18.4, 18.4,8.1,8.3]
        width_init_driver0 = [20,20,20,20]
        amplitude_init_driver1 = [9.1, 9.4, 8.3, 8.3]
        width_init_driver1 = [20,20,20,20] 
    elif var_module.get()=="WPoly":
        amplitude_init_driver0 = [10.05, 15.95,10.30,12.2]
        width_init_driver0 = [15,15,15,15]
        #not used for this module, but we have to give them some values for consistency with the code below
        amplitude_init_driver1 = [10, 10, 10, 10]
        width_init_driver1 = [20,20,20,20] 
    elif var_module.get()=="Def1":
        # retrieve information from the text file, if the text file exists
        if(exists("./default1.txt")):
            print("default 1 found")
            (amplitude_init_driver0,width_init_driver0,amplitude_init_driver1,width_init_driver1) = np.loadtxt("./default1.txt",float)
            #print(amp,width,amp_d1,width_d1)
        else:
            print("default 1 not found")
    # setting the values (driver0)
    for i in np.arange(0,4):
        width_init_driver0[i] = width_init_driver1[i] / 1E+9
        width_init_driver1[i] = width_init_driver1[i] / 1E+9
        obj_string_set_amp = "LED_driver_write(0,'ch" + str(i+1) + "','amp', amplitude_init_driver0[" + str(i) + "])"
        obj_string_set_width = "LED_driver_write(0,'ch" + str(i+1) + "','width', width_init_driver0[" + str(i) + "])"
        obj_string_set_amp_d1 = "LED_driver_write(1,'ch" + str(i+1) + "','amp', amplitude_init_driver1[" + str(i) + "])"
        obj_string_set_width_d1 = "LED_driver_write(1,'ch" + str(i+1) + "','width', width_init_driver1[" + str(i) + "])"
        status_string_set_amp = "status_ch" + str(i+1) + "_amp "
        status_string_set_width = "status_ch" + str(i+1) + "_width"
        status_string_set_amp_d1 = "status_ch" + str(i+1) + "_amp_driver1"
        status_string_set_width_d1 ="status_ch" + str(i+1) + "_width_driver1"

        if eval(obj_string_set_amp) & eval(obj_string_set_width) & eval(obj_string_set_amp_d1) & eval(obj_string_set_width_d1):
            eval(status_string_set_amp)['text'] =  amplitude_init_driver0[i]
            eval(status_string_set_amp)['fg'] = 'green'
            eval(status_string_set_width)['text'] =  width_init_driver0[i]
            eval(status_string_set_width)['fg'] = 'green'
            eval(status_string_set_amp_d1)['text'] =  amplitude_init_driver1[i]
            eval(status_string_set_amp_d1)['fg'] = 'green'
            eval(status_string_set_width_d1)['text'] =  width_init_driver1[i]
            eval(status_string_set_width_d1)['fg'] = 'green'
        else:
            eval(status_string_set_amp)['text'] = (' err')
            eval(status_string_set_amp)['fg'] = 'red'
            eval(status_string_set_width)['text'] =  (' err')
            eval(status_string_set_width)['fg'] = 'red'
            eval(status_string_set_amp_d1)['text'] =  (' err')
            eval(status_string_set_amp_d1)['fg'] = 'red'
            eval(status_string_set_width_d1)['text'] =  (' err')
            eval(status_string_set_width_d1)['fg'] = 'red'

def save_default1():
    amp = np.zeros(4)
    width = np.zeros(4)
    amp_d1 = np.zeros(4)
    width_d1 = np.zeros(4)
    for i in np.arange(0,4):
        obj_string_get_amp = "ent_ch" + str(i+1) + "_amp"
        obj_string_get_width = "ent_ch" + str(i+1) + "_width"
        obj_string_get_amp_d1 = "ent_ch" + str(i+1) + "_amp_driver1"
        obj_string_get_width_d1 = "ent_ch" + str(i+1) + "_width_driver1"
        amp[i] = ent_ch2_amp.get()
        width[i] = eval(obj_string_get_width).get()
        amp_d1[i] = eval(obj_string_get_amp_d1).get()
        width_d1[i] = eval(obj_string_get_width_d1).get()
    np.savetxt('default1.txt', (amp,width,amp_d1,width_d1))

def toggle_driver1():
    if switch_driver0.config('text')[-1] == 'ON':
        switch_driver0.config(text='OFF')
        switch_driver0["bg"]="red"
        switch_driver0["fg"]="black"
        LED_driver_write(0,'main','status','0')
        switch_ch1.config(text='OFF')
        LED_driver_write(0,'ch1','status','0')
        switch_ch2.config(text='OFF')
        LED_driver_write(0,'ch2','status','0')
        switch_ch3.config(text='OFF')
        LED_driver_write(0,'ch3','status','0')
        switch_ch4.config(text='OFF')
        LED_driver_write(0,'ch4','status','0')
    else:
        switch_driver0.config(text='ON')
        switch_driver0["bg"]="green"
        switch_driver0["fg"]="white"
        LED_driver_write(0,'main','status','1')

def toggle_driver2():
    if switch_driver1.config('text')[-1] == 'ON':
        switch_driver1.config(text='OFF')
        switch_driver1["bg"]="red"
        switch_driver1["fg"]="black"
        LED_driver_write(1,'main','status','0')
        switch_ch1_driver1.config(text='OFF')
        LED_driver_write(1,'ch1','status','0')
        switch_ch2_driver1.config(text='OFF')
        LED_driver_write(1,'ch2','status','0')
        switch_ch3_driver1.config(text='OFF')
        LED_driver_write(1,'ch3','status','0')
        switch_ch4_driver1.config(text='OFF')
        LED_driver_write(1,'ch4','status','0')

    else:
        switch_driver1.config(text='ON')
        switch_driver1["bg"]="green"
        switch_driver1["fg"]="white"
        LED_driver_write(1,'main','status','1')

def toggle_ch1():
    if switch_ch1.config('text')[-1] == 'ON':
        switch_ch1.config(text='OFF')
        LED_driver_write(0,'ch1','status','0')
    else:
        switch_ch1.config(text='ON')
        LED_driver_write(0,'ch1','status','1')

def set_ch1_amp():
    ch1_amp = ent_ch1_amp.get()
    success_return = LED_driver_write(0,'ch1','amp', ch1_amp)
    if success_return:
        status_ch1_amp["text"] = (ch1_amp)
        status_ch1_amp["fg"] = "green"
    else:
        status_ch1_amp["text"] = (" err")
        status_ch1_amp["fg"] = "red"

def set_ch1_width():
    ch1_width = str(float(ent_ch1_width.get())/1000000000)
    success_return = LED_driver_write(0,'ch1','width', ch1_width)
    if success_return:
        status_ch1_width["text"] = (ch1_width)
        status_ch1_width["fg"] = "green"
    else:
        status_ch1_width["text"] = (" err")
        status_ch1_width["fg"] = "red"

def toggle_ch2():
    if switch_ch2.config('text')[-1] == 'ON':
        switch_ch2.config(text='OFF')
        LED_driver_write(0,'ch2','status','0')
    else:
        switch_ch2.config(text='ON')
        LED_driver_write(0,'ch2','status','1')

def set_ch2_amp():
    ch2_amp = ent_ch2_amp.get()
    success_return = LED_driver_write(0,'ch2','amp', ch2_amp)
    if success_return:
        status_ch2_amp["text"] = (ch2_amp)
        status_ch2_amp["fg"] = "green"
    else:
        status_ch2_amp["text"] = (" err")
        status_ch2_amp["fg"] = "red"

def set_ch2_width():
    ch2_width = str(float(ent_ch2_width.get())/1000000000)
    success_return = LED_driver_write(0,'ch2','width', ch2_width)
    if success_return:
        status_ch2_width["text"] = (ch2_width)
        status_ch2_width["fg"] = "green"
    else:
        status_ch2_width["text"] = (" err")
        status_ch2_width["fg"] = "red"

def toggle_ch3():
    if switch_ch3.config('text')[-1] == 'ON':
        switch_ch3.config(text='OFF')
        LED_driver_write(0,'ch3','status','0')
    else:
        switch_ch3.config(text='ON')
        LED_driver_write(0,'ch3','status','1')

def set_ch3_amp():
    ch3_amp = ent_ch3_amp.get()
    success_return = LED_driver_write(0,'ch3','amp', ch3_amp)
    if success_return:
        status_ch3_amp["text"] = (ch3_amp)
        status_ch3_amp["fg"] = "green"
    else:
        status_ch3_amp["text"] = (" err")
        status_ch3_amp["fg"] = "red"

def set_ch3_width():
    ch3_width = str(float(ent_ch3_width.get())/1000000000)
    success_return = LED_driver_write(0,'ch3','width', ch3_width)
    if success_return:
        status_ch3_width["text"] = (ch3_width)
        status_ch3_width["fg"] = "green"

    else:
        status_ch3_width["text"] = (" err")
        status_ch3_width["fg"] = "red"

def toggle_ch4():
    if switch_ch4.config('text')[-1] == 'ON':
        switch_ch4.config(text='OFF')
        LED_driver_write(0,'ch4','status','0')
    else:
        switch_ch4.config(text='ON')
        LED_driver_write(0,'ch4','status','1')

def set_ch4_amp():
    ch4_amp = ent_ch4_amp.get()
    success_return = LED_driver_write(0,'ch4','amp', ch4_amp)
    if success_return:
        status_ch4_amp["text"] = (ch4_amp)
        status_ch4_amp["fg"] = "green"
    else:
        status_ch4_amp["text"] = (" err")
        status_ch4_amp["fg"] = "red"

def set_ch4_width():
    ch4_width = str(float(ent_ch4_width.get())/1000000000)
    success_return = LED_driver_write(0,'ch4','width', ch4_width)
    if success_return:
        status_ch4_width["text"] = (ch4_width)
        status_ch4_width["fg"] = "green"
    else:
        status_ch4_width["text"] = (" err")
        status_ch4_width["fg"] = "red"

def toggle_ch1_driver1():
    if switch_ch1_driver1.config('text')[-1] == 'ON':
        switch_ch1_driver1.config(text='OFF')
        LED_driver_write(1,'ch1','status','0')
    else:
        switch_ch1_driver1.config(text='ON')
        LED_driver_write(1,'ch1','status','1')

def set_ch1_amp_driver1():
    ch1_amp_driver1 = ent_ch1_amp_driver1.get()
    success_return = LED_driver_write(1,'ch1','amp', ch1_amp_driver1)
    if success_return:
        status_ch1_amp_driver1["text"] = (ch1_amp_driver1)
        status_ch1_amp_driver1["fg"] = "green"
    else:
        status_ch1_amp_driver1["text"] = (" err")
        status_ch1_amp_driver1["fg"] = "red"

def set_ch1_width_driver1():
    ch1_width_driver1 = str(float(ent_ch1_width_driver1.get())/1000000000)
    success_return = LED_driver_write(1,'ch1','width', ch1_width_driver1)
    if success_return:
        status_ch1_width_driver1["text"] = (ch1_width_driver1)
    else:
        status_ch1_width_driver1["text"] = (" err")
        status_ch1_width_driver1["fg"] = "red"

def toggle_ch2_driver1():
    if switch_ch2_driver1.config('text')[-1] == 'ON':
        switch_ch2_driver1.config(text='OFF')
        LED_driver_write(1,'ch2','status','0')
    else:
        switch_ch2_driver1.config(text='ON')
        LED_driver_write(1,'ch2','status','1')

def set_ch2_amp_driver1():
    ch2_amp_driver1 = ent_ch2_amp_driver1.get()
    success_return = LED_driver_write(1,'ch2','amp', ch2_amp_driver1)
    if success_return:
        status_ch2_amp_driver1["text"] = (ch2_amp_driver1)
        status_ch2_amp_driver1["fg"] = "green"
    else:
        status_ch2_amp_driver1["text"] = (" err")
        status_ch2_amp_driver1["fg"] = "red"

def set_ch2_width_driver1():
    ch2_width_driver1 = str(float(ent_ch2_width_driver1.get())/1000000000)
    success_return = LED_driver_write(1,'ch2','width', ch2_width_driver1)
    if success_return:
        status_ch2_width_driver1["text"] = (ch2_width_driver1)
        status_ch2_width_driver1["fg"] = "green"
    else:
        status_ch2_width_driver1["text"] = (" err")
        status_ch2_width_driver1["fg"] = "red"

def toggle_ch3_driver1():
    if switch_ch3_driver1.config('text')[-1] == 'ON':
        switch_ch3_driver1.config(text='OFF')
        LED_driver_write(1,'ch3','status','0')
    else:
        switch_ch3_driver1.config(text='ON')
        LED_driver_write(1,'ch3','status','1')

def set_ch3_amp_driver1():
    ch3_amp_driver1 = ent_ch3_amp_driver1.get()
    success_return = LED_driver_write(1,'ch3','amp', ch3_amp_driver1)
    if success_return:
        status_ch3_amp_driver1["text"] = (ch3_amp_driver1)
        status_ch3_amp_driver1["fg"] = "green"
    else:
        status_ch3_amp_driver1["text"] = (" err")
        status_ch3_amp_driver1["fg"] = "red"

def set_ch3_width_driver1():
    ch3_width_driver1 = str(float(ent_ch3_width_driver1.get())/1000000000)
    success_return = LED_driver_write(1,'ch3','width', ch3_width_driver1)
    if success_return:
        status_ch3_width_driver1["text"] = (ch3_width_driver1)
        status_ch3_width_driver1["fg"] = "green"
    else:
        status_ch3_width_driver1["text"] = (" err")
        status_ch3_width_driver1["fg"] = "red"

def toggle_ch4_driver1():
    if switch_ch4_driver1.config('text')[-1] == 'ON':
        switch_ch4_driver1.config(text='OFF')
        LED_driver_write(1,'ch4','status','0')
    else:
        switch_ch4_driver1.config(text='ON')
        LED_driver_write(1,'ch4','status','1')

def set_ch4_amp_driver1():
    ch4_amp_driver1 = ent_ch4_amp_driver1.get()
    success_return = LED_driver_write(1,'ch4','amp', ch4_amp_driver1)
    if success_return:
        status_ch4_amp_driver1["text"] = (ch4_amp_driver1)
        status_ch4_amp_driver1["fg"] = "green"
    else:
        status_ch4_amp_driver1["text"] = (" err")
        status_ch4_amp_driver1["fg"] = "red"

def set_ch4_width_driver1():
    ch4_width_driver1 = str(float(ent_ch4_width_driver1.get())/1000000000)
    success_return = LED_driver_write(1,'ch4','width', ch4_width_driver1)
    if success_return:
        status_ch4_width_driver1["text"] = (ch4_width_driver1)
        status_ch4_width_driver1["fg"] = "green"
    else:
        status_ch4_width_driver1["text"] = (" err")
        status_ch4_width_driver1["fg"] = "red"

def increase():
    value = int(lbl_value["text"])
    lbl_value["text"] = f"{value + 1}"

def decrease():
    value = int(lbl_value["text"])
    lbl_value["text"] = f"{value - 1}"

def altitude():
    angle_altitude = int(ent_altitude.get())
    print(angle_altitude)
    if motors:
        with Connection.open_serial_port("/dev/ZaberMotors") as connection:
            device_list = connection.detect_devices()
            device = device_list[0] # device 1 is rotational stage, 0 is the linear stage.
            axis = device.get_axis(1)
            axis.unpark()
            if angle_altitude in listOfAngles:
                axis.move_absolute(int(altitude_zero-(altitude_step*angle_altitude)), unit = Units.NATIVE, wait_until_idle = True)
                axis.park()
                print("Angle {} reached over Altitude...".format(angle_altitude))
                print("Motor is now in 'park' position.")
            else:
                # axis.move_absolute(angle, unit = Units.NATIVE, wait_until_idle = True)
                # axis.park()
                print("altitude() only accept integer number from 0 to 6.")
                #return "Motor not moved. Please select a suitable angle (integer 1° -> 6°)"
        lbl_result_altitude["text"] = (angle_altitude ,"°")
        lbl_result_altitude["fg"] = "green"

    else:
        print("Warning: motor flag not active")
        lbl_result_altitude["text"] = ("err")
        lbl_result_altitude["fg"] = "red"

def azimuth():
    angle_azimuth = int(ent_azimuth.get())
    if motors:
        with Connection.open_serial_port("/dev/ZaberMotors") as connection:
            device_list = connection.detect_devices()
            device = device_list[1] # device 1 is rotational stage, 0 is the linear stage.
            axis = device.get_axis(1)
            axis.unpark()
            if angle_azimuth in listOfAngles:                
                axis.move_absolute(azimuth_zero+angle_azimuth, unit = Units.ANGLE_DEGREES, wait_until_idle = True)
                axis.park()
                print("Angle {} reached over Azimuth...".format(angle_azimuth))
                print("Motor is now in 'park' position.")
            else:
                # axis.move_absolute(angle, unit = Units.NATIVE, wait_until_idle = True)
                # axis.park()
                print("azimuth() only accept integer number from 0 to 6.")
                #return "Motor not moved. Please select a suitable angle (integer 1° -> 6°)"
        lbl_result_azimuth["text"] = (angle_azimuth, "°")
        lbl_result_azimuth["fg"] = "green"

    else:
        print("Warning: motor flag not active")
        lbl_result_azimuth["text"] = ("err")
        lbl_result_azimuth["fg"] = "red"
        
def att1_toggle_10dB():
    if attenuators:
        if btn_att1_10dB.config('bg')[-1] == 'grey':
            btn_att1_10dB.config(bg='yellow')
            board.digital_write(4,1)
            time.sleep(0.5)
            board.digital_write(4,0)
        else:
            btn_att1_10dB.config(bg='grey')
            board.digital_write(7,1)
            time.sleep(0.5)
            board.digital_write(7,0)
    else:
        print("Warning: attenuators flag not active")

def att1_toggle_20dB():
    if attenuators:
        if btn_att1_20dB.config('bg')[-1] == 'grey':
            btn_att1_20dB.config(bg='yellow')
            board.digital_write(2,1)
            time.sleep(0.5)
            board.digital_write(2,0)
        else:
            btn_att1_20dB.config(bg='grey')
            board.digital_write(6,1)
            time.sleep(0.5)
            board.digital_write(6,0)
    else:
        print("Warning: attenuators flag not active")

def att1_toggle_40dB():
    if attenuators:
        if btn_att1_40dB.config('bg')[-1] == 'grey':
            btn_att1_40dB.config(bg='yellow')
            board.digital_write(5,1)
            time.sleep(0.5)
            board.digital_write(5,0)
        else:
            btn_att1_40dB.config(bg='grey')
            board.digital_write(3,1)
            time.sleep(0.5)
            board.digital_write(3,0)
    else:
        print("Warning: attenuators flag not active")


def att2_toggle_10dB():
    if attenuators:
        if btn_att2_10dB.config('bg')[-1] == 'grey':
            btn_att2_10dB.config(bg='yellow')
            board.digital_write(9,1)
            time.sleep(0.5)
            board.digital_write(9,0)
        else:
            btn_att2_10dB.config(bg='grey')
            board.digital_write(8,1)
            time.sleep(0.5)
            board.digital_write(8,0)
    else:
        print("Warning: attenuators flag not active")

def att2_toggle_20dB():
    if attenuators:
        if btn_att2_20dB.config('bg')[-1] == 'grey':
            btn_att2_20dB.config(bg='yellow')
            board.digital_write(10,1)
            time.sleep(0.5)
            board.digital_write(10,0)
        else:
            btn_att2_20dB.config(bg='grey')
            board.digital_write(12,1)
            time.sleep(0.5)
            board.digital_write(12,0)
    else:
        print("Warning: attenuators flag not active")

def att2_toggle_40dB():
    if attenuators:
        if btn_att2_40dB.config('bg')[-1] == 'grey':
            btn_att2_40dB.config(bg='yellow')
            board.digital_write(14,1)
            time.sleep(0.5)
            board.digital_write(14,0)
        else:
            btn_att2_40dB.config(bg='grey')
            board.digital_write(11,1)
            time.sleep(0.5)
            board.digital_write(11,0)
    else:
        print("Warning: attenuators flag not active")

## ⚙️⚙️⚙️ ZABER motors init ⚙️⚙️⚙️ ##
#intialize and open zaber devices (2)
def motors_init():
    Library.enable_device_db_store()
    global device_list
    with Connection.open_serial_port("/dev/ZaberMotors") as connection:
        device_list = connection.detect_devices()
        #print("Found {} devices".format(len(device_list)))

    if len(device_list)!= 2:
        print("Found {} devices whereas 2 were expected.".format(len(device_list)))
    #Setting some fixed parameters..
    global altitude_zero
    global azimuth_zero
    global altitude_step
    global listOfAngles
    altitude_zero = 835500 #zero here defines the step number where the mechanics is at zero
    azimuth_zero = 88.6
    altitude_step = 128000/6 # number of steps for 1 degree [°]. 128000 was empirically measured between 0° and 6° 
    listOfAngles = [-6 ,-5 ,-4 ,-3 ,-2 ,-1 ,0 ,1 ,2 ,3 ,4 ,5 ,6]


## ░░ LED drivers init ░░ ##

def LEDs_init():
    print("LEDs initialization ...")
    rm = pyvisa.ResourceManager()
    global driver_list 
    driver_list = [0,0]
    # perform some checks on the number of devices connected?
    # the following method assumes that rules has been created for the LED drivers with names LEDdriver0 and LEDdriver1
    driver_list[0]=rm.open_resource('ASRL/dev/LEDdriver0::INSTR')
    driver_list[1]=rm.open_resource('ASRL/dev/LEDdriver1::INSTR')
    # for driver in driver_list:
    driver_list[0].baud_rate = 115200
    driver_list[0].read_termination = '\n'
    driver_list[0].write_termination = '\n'
    print(" ... done")
    

## ⛃⛃⛃ Remote attenuators init ⛃⛃⛃ ##
## pin MAP from arduino to attenutor sections, arduino digital pin 2->7 for att 1, 8->12(dig) + 14(analog) for att 2
## section 1 = 10 dB. att 1: ON pin 4, OFF pin 7; att 2: ON pin 9, OFF pin 8
## section 2 = 20 dB. att 1: ON pin 2, OFF pin 6; att 2: ON pin 10, OFF pin 12
## section 3 = 40 dB. att 1: ON pin 5, OFF pin 3; att 2: ON pin 14, OFF pin 11
def attenuators_init():
    board = PyMata("/dev/Attenuators")
    board.set_pin_mode(14, board.OUTPUT, board.DIGITAL) # 14 is the only analog pin.
                                                        # all the other pins do not need any special setting.

#### 🗔 general GUI 🗔 ####
# tools for the global GUI
window = tk.Tk()
window.title('TB LHCb ECAL UII GUI')
window.rowconfigure(0, minsize=20, weight=1)
window.columnconfigure([0, 1, 2], minsize=15, weight=1)
#common frame
frm_entry = tk.Frame(master=window)

# select the connected harware 
motors_select = tk.IntVar()
motors_selector = tk.Checkbutton(master=frm_entry, text="mot", variable=motors_select, command = motors_init)
LEDs_select = tk.IntVar()
LEDs_selector = tk.Checkbutton(master=frm_entry, text="LED", variable=LEDs_select, command = LEDs_init)
attenuators_select = tk.IntVar()
attenuators_selector = tk.Checkbutton(master=frm_entry, text="att", variable=attenuators_select, command = attenuators_init)
# useful flags
motors =      motors_select  
LED =         LEDs_select
attenuators = attenuators_select 

#### GUI for all the devices ####
### 🗔⚙️ GUI: motors ###

# main label motors
lbl_motors = tk.Label(master=frm_entry, text="Motors", font='Monserrat 10 bold')
lbl_motors_status = tk.Label(master=frm_entry, text="status", font='Monserrat 8')
# azimuth entry and labels
lbl_azimuth = tk.Label(master=frm_entry, text="azimuth", font='Monserrat 8')
ent_azimuth = tk.Entry(master=frm_entry, width=4)
lbl_azimuth_unit = tk.Label(master=frm_entry, text="°")
lbl_result_azimuth = tk.Label(master=frm_entry, text="-", font='Monserrat 8')
# altitude entry and labels
lbl_altitude = tk.Label(master=frm_entry, text="altitude",font='Monserrat 8')
ent_altitude= tk.Entry(master=frm_entry, width=4)
lbl_altitude_unit = tk.Label(master=frm_entry, text="°")
lbl_result_altitude = tk.Label(master=frm_entry, text="-", font='Monserrat 8')
# buttons
btn_azimuth_set = tk.Button(master=frm_entry,text="set",command=azimuth)
btn_altitude_set = tk.Button(master=frm_entry,text="set",command=altitude)
# result label

### 🗔░ GUI: LEDs ###

# main label LEDs
lbl_LEDs = tk.Label(master=frm_entry, text="LEDs",  font='Monserrat 10 bold')
lbl_driver0 = tk.Label(master=frm_entry, text="MAIN0", font='Helvetica 10 ')
lbl_driver1 = tk.Label(master=frm_entry, text="MAIN1", font='Monserrat 10 ')
# ON/OFF main switch for each driver
switch_driver0 = tk.Button(master=frm_entry, bg = "red", text="OFF", width=3, command=toggle_driver1)
switch_driver1 = tk.Button(master=frm_entry, bg = "red", text="OFF", width=3, command=toggle_driver2)
# channel status (switch ON/OFF) + amp + width
# technology selection
var_module = tk.StringVar()
spacalWgagg_check = tk.Radiobutton(master=frm_entry, 
                   indicatoron = 0,
                   text="W-GAGG",
                   padx = 20, 
                   command=spacal_lab_default,
                   variable=var_module,
                   value="Wgagg")
spacalPbPoly_check = tk.Radiobutton(master=frm_entry, 
                   indicatoron = 0,
                   text="Pb-poly",
                   padx = 20, 
                   command=spacal_lab_default, 
                   variable=var_module,
                   value="PbPoly") 
spacalWPoly_check = tk.Radiobutton(master=frm_entry, 
                   indicatoron = 0,
                   text="W-poly",
                   padx = 20, 
                   command=spacal_lab_default, 
                   variable=var_module,
                   value="WPoly") 
# and default selection, in the same button exclusivity group of the above technologies
save_def1 = tk.Button(master=frm_entry, text="save", width=3, command=save_default1)
def1_label = tk.Label(master=frm_entry, text="default1", font='Helvetica 10 ')
spacalDefault1_check = tk.Radiobutton(master=frm_entry, 
                   indicatoron = 0,
                   text="set",
                   padx = 20, 
                   command=spacal_lab_default, 
                   variable=var_module,
                   value="Def1") 
spacalDefault2_check = tk.Radiobutton(master=frm_entry, 
                   indicatoron = 0,
                   text="set",
                   padx = 20, 
                   command=spacal_lab_default, 
                   variable=var_module,
                   value="Def2") 

## driver 0
lbl_driver0_status = tk.Label(master=frm_entry, text="status", font='Monserrat 8')
lbl_driver0_status_width = tk.Label(master=frm_entry, text="status        ", font='Monserrat 8')
### channel 1 (A)
#### ON/OFF
lbl_ch1 = tk.Label(master=frm_entry, text="ch1")
switch_ch1 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch1)
#### amp
lbl_ch1_amp = tk.Label(master=frm_entry, text="amp V")
ent_ch1_amp = tk.Entry(master=frm_entry, width=4)
btn_ch1_amp = tk.Button(master=frm_entry,text="set",command=set_ch1_amp)
status_ch1_amp = tk.Label(master=frm_entry, text="-", font='Monserrat 8')
#### width
lbl_ch1_width = tk.Label(master=frm_entry, text="width")
ent_ch1_width = tk.Entry(master=frm_entry, width=4)
btn_ch1_width = tk.Button(master=frm_entry,text="set",command=set_ch1_width)
status_ch1_width = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### channel 2 (B)
#### ON/OFF
lbl_ch2 = tk.Label(master=frm_entry, text="ch2")
switch_ch2 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch2)
#### amp
lbl_ch2_amp = tk.Label(master=frm_entry, text="amp V")
ent_ch2_amp = tk.Entry(master=frm_entry, width=4)
btn_ch2_amp = tk.Button(master=frm_entry,text="set",command=set_ch2_amp)
status_ch2_amp = tk.Label(master=frm_entry, text="-", font='Monserrat 8')
#### width
lbl_ch2_width = tk.Label(master=frm_entry, text="width")
ent_ch2_width = tk.Entry(master=frm_entry, width=4)
btn_ch2_width = tk.Button(master=frm_entry,text="set",command=set_ch2_width)
status_ch2_width = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### channel 3 (C)
#### ON/OFF
lbl_ch3 = tk.Label(master=frm_entry, text="ch3")
switch_ch3 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch3)
#### amp
lbl_ch3_amp = tk.Label(master=frm_entry, text="amp V")
ent_ch3_amp = tk.Entry(master=frm_entry, width=4)
btn_ch3_amp = tk.Button(master=frm_entry,text="set",command=set_ch3_amp)
status_ch3_amp = tk.Label(master=frm_entry, text="-", font='Monserrat 8')
#### width
lbl_ch3_width = tk.Label(master=frm_entry, text="width")
ent_ch3_width = tk.Entry(master=frm_entry, width=4)
btn_ch3_width = tk.Button(master=frm_entry,text="set",command=set_ch3_width)
status_ch3_width = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### channel 4 (D)
#### ON/OFF
lbl_ch4 = tk.Label(master=frm_entry, text="ch4")
switch_ch4 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch4)
#### amp
lbl_ch4_amp = tk.Label(master=frm_entry, text="amp V")
ent_ch4_amp = tk.Entry(master=frm_entry, width=4)
btn_ch4_amp = tk.Button(master=frm_entry,text="set",command=set_ch4_amp)
status_ch4_amp = tk.Label(master=frm_entry, text="-", font='Monserrat 8')
#### width
lbl_ch4_width = tk.Label(master=frm_entry, text="width")
ent_ch4_width = tk.Entry(master=frm_entry, width=4)
btn_ch4_width = tk.Button(master=frm_entry,text="set",command=set_ch4_width)
status_ch4_width = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
## driver 1
lbl_driver1_status = tk.Label(master=frm_entry, text="status    ", font='Monserrat 8')
lbl_driver1_status_width = tk.Label(master=frm_entry, text="status    ", font='Monserrat 8')
### channel 1 (A)
#### ON/OFF
lbl_ch1_driver1 = tk.Label(master=frm_entry, text="ch1")
switch_ch1_driver1 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch1_driver1)
#### amp
lbl_ch1_amp_driver1 = tk.Label(master=frm_entry, text="amp V")
ent_ch1_amp_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch1_amp_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch1_amp_driver1)
status_ch1_amp_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
#### width
lbl_ch1_width_driver1 = tk.Label(master=frm_entry, text="width")
ent_ch1_width_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch1_width_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch1_width_driver1)
status_ch1_width_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### channel 2 (B)
#### ON/OFF
lbl_ch2_driver1 = tk.Label(master=frm_entry, text="ch2")
switch_ch2_driver1 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch2_driver1)
#### amp
lbl_ch2_amp_driver1 = tk.Label(master=frm_entry, text="amp V")
ent_ch2_amp_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch2_amp_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch2_amp_driver1)
status_ch2_amp_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
#### width
lbl_ch2_width_driver1 = tk.Label(master=frm_entry, text="width")
ent_ch2_width_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch2_width_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch2_width_driver1)
status_ch2_width_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### channel 3 (C)
#### ON/OFF
lbl_ch3_driver1 = tk.Label(master=frm_entry, text="ch3")
switch_ch3_driver1 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch3_driver1)
#### amp
lbl_ch3_amp_driver1 = tk.Label(master=frm_entry, text="amp V")
ent_ch3_amp_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch3_amp_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch3_amp_driver1)
status_ch3_amp_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
#### width
lbl_ch3_width_driver1 = tk.Label(master=frm_entry, text="width")
ent_ch3_width_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch3_width_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch3_width_driver1)
status_ch3_width_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### channel 4 (D)
#### ON/OFF
lbl_ch4_driver1 = tk.Label(master=frm_entry, text="ch4")
switch_ch4_driver1 = tk.Button(master=frm_entry, text="OFF", width=3, command=toggle_ch4_driver1)
#### amp
lbl_ch4_amp_driver1 = tk.Label(master=frm_entry, text="amp V")
ent_ch4_amp_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch4_amp_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch4_amp_driver1)
status_ch4_amp_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
#### width
lbl_ch4_width_driver1 = tk.Label(master=frm_entry, text="width")
ent_ch4_width_driver1 = tk.Entry(master=frm_entry, width=4)
btn_ch4_width_driver1 = tk.Button(master=frm_entry,text="set",command=set_ch4_width_driver1)
status_ch4_width_driver1 = tk.Label(master=frm_entry, text=" -", font='Monserrat 8')
### 🗔⚙️ GUI: attenuators ###

# main label attenuators
lbl_att = tk.Label(master=frm_entry, text="dB", font='Monserrat 10 bold')
lbl_att1 = tk.Label(master=frm_entry, text="Front ", font='Monserrat 9')
lbl_att2 = tk.Label(master=frm_entry, text="Back ", font='Monserrat 9')

# buttons
btn_att1_10dB = tk.Button(master=frm_entry, bg = "grey", text="10dB",command=att1_toggle_10dB)
btn_att1_20dB = tk.Button(master=frm_entry, bg = "grey", text="20dB",command=att1_toggle_20dB)
btn_att1_40dB = tk.Button(master=frm_entry, bg = "grey", text="40dB",command=att1_toggle_40dB)
btn_att2_10dB = tk.Button(master=frm_entry, bg = "grey", text="10dB",command=att2_toggle_10dB)
btn_att2_20dB = tk.Button(master=frm_entry, bg = "grey", text="20dB",command=att2_toggle_20dB)
btn_att2_40dB = tk.Button(master=frm_entry, bg = "grey", text="40dB",command=att2_toggle_40dB)



#####    building the grid    #####
# motors
lbl_motors.grid(row=0, column=3)
lbl_motors_status.grid(row=1, column=4)
lbl_azimuth.grid(row=2, column=2)
ent_azimuth.grid(row=2, column=0, sticky="w")
lbl_azimuth_unit.grid(row=2, column=1, sticky="w")
lbl_azimuth.grid(row=2, column=2, sticky="w")
ent_altitude.grid(row=3, column=0, sticky="w")
lbl_altitude_unit.grid(row=3, column=1, sticky="w")
lbl_altitude.grid(row=3, column=2, sticky="w")
frm_entry.grid(row=2, column=0, padx=10)
btn_azimuth_set.grid(row=2, column=3, pady=10)
lbl_result_azimuth.grid(row=2, column=4)
btn_altitude_set.grid(row=3, column=3, pady=10)
lbl_result_altitude.grid(row=3, column=4)
# LEDs
# LED label
lbl_LEDs.grid(row=4, column=3)
#technology selection
spacalWgagg_check.grid(row=4, column=4)
spacalPbPoly_check.grid(row=4, column=5)
spacalWPoly_check.grid(row=4, column=6)
def1_label.grid(row=4, column=7)
save_def1.grid(row=4, column=8)
spacalDefault1_check.grid(row=4, column=9)
#spacalDefault2_check.grid(row=4, column=12)
# defult extra buttons

# main
lbl_driver0.grid(row=5, column=0, sticky="w")
lbl_driver1.grid(row=5, column=9, sticky="w")
switch_driver0.grid(row=5, column=1, pady=10)
switch_driver1.grid(row=5, column=10, pady=10)
# driver 0
lbl_driver0_status.grid(row=6, column=3, sticky="w")
lbl_driver0_status_width.grid(row=6, column=7, sticky="w")
## channel 1
lbl_ch1.grid(row=7, column=0, sticky="w")
switch_ch1.grid(row=7, column=1, sticky="w")
lbl_ch1_amp.grid(row=8, column=0, sticky="w")
ent_ch1_amp.grid(row=8, column=1, sticky="w")
btn_ch1_amp.grid(row=8, column=2, sticky="w")
status_ch1_amp.grid(row=8, column=3, sticky="w")
lbl_ch1_width.grid(row=8, column=4, sticky="w")
ent_ch1_width.grid(row=8, column=5, sticky="w")
btn_ch1_width.grid(row=8, column=6, sticky="w")
status_ch1_width.grid(row=8, column=7, sticky="w")
## channel 2
lbl_ch2.grid(row=9, column=0, sticky="w")
switch_ch2.grid(row=9, column=1, sticky="w")
lbl_ch2_amp.grid(row=10, column=0, sticky="w")
ent_ch2_amp.grid(row=10, column=1, sticky="w")
btn_ch2_amp.grid(row=10, column=2, sticky="w")
status_ch2_amp.grid(row=10, column=3, sticky="w")
lbl_ch2_width.grid(row=10, column=4, sticky="w")
ent_ch2_width.grid(row=10, column=5, sticky="w")
btn_ch2_width.grid(row=10, column=6, sticky="w")
status_ch2_width.grid(row=10, column=7, sticky="w")
## channel 3
lbl_ch3.grid(row=11, column=0, sticky="w")
switch_ch3.grid(row=11, column=1, sticky="w")
lbl_ch3_amp.grid(row=12, column=0, sticky="w")
ent_ch3_amp.grid(row=12, column=1, sticky="w")
btn_ch3_amp.grid(row=12, column=2, sticky="w")
status_ch3_amp.grid(row=12, column=3, sticky="w")
lbl_ch3_width.grid(row=12, column=4, sticky="w")
ent_ch3_width.grid(row=12, column=5, sticky="w")
btn_ch3_width.grid(row=12, column=6, sticky="w")
status_ch3_width.grid(row=12, column=7, sticky="w")
## channel 4
lbl_ch4.grid(row=13, column=0, sticky="w")
switch_ch4.grid(row=13, column=1, sticky="w")
lbl_ch4_amp.grid(row=14, column=0, sticky="w")
ent_ch4_amp.grid(row=14, column=1, sticky="w")
btn_ch4_amp.grid(row=14, column=2, sticky="w")
status_ch4_amp.grid(row=14, column=3, sticky="w")
lbl_ch4_width.grid(row=14, column=4, sticky="w")
ent_ch4_width.grid(row=14, column=5, sticky="w")
btn_ch4_width.grid(row=14, column=6, sticky="w")
status_ch4_width.grid(row=14, column=7, sticky="w")
# driver 1
lbl_driver1_status.grid(row=6, column=12, sticky="w")
lbl_driver1_status_width.grid(row=6, column=16, sticky="w")
## channel 1
lbl_ch1_driver1.grid(row=7, column=9, sticky="w")
switch_ch1_driver1.grid(row=7, column=10, sticky="w")
lbl_ch1_amp_driver1.grid(row=8, column=9, sticky="w")
ent_ch1_amp_driver1.grid(row=8, column=10, sticky="w")
btn_ch1_amp_driver1.grid(row=8, column=11, sticky="w")
status_ch1_amp_driver1.grid(row=8, column=12, sticky="w")
lbl_ch1_width_driver1.grid(row=8, column=13, sticky="w")
ent_ch1_width_driver1.grid(row=8, column=14, sticky="w")
btn_ch1_width_driver1.grid(row=8, column=15, sticky="w")
status_ch1_width_driver1.grid(row=8, column=16, sticky="w")
## channel 2
lbl_ch2_driver1.grid(row=9, column=9, sticky="w")
switch_ch2_driver1.grid(row=9, column=10, sticky="w")
lbl_ch2_amp_driver1.grid(row=10, column=9, sticky="w")
ent_ch2_amp_driver1.grid(row=10, column=10, sticky="w")
btn_ch2_amp_driver1.grid(row=10, column=11, sticky="w")
status_ch2_amp_driver1.grid(row=10, column=12, sticky="w")
lbl_ch2_width_driver1.grid(row=10, column=13, sticky="w")
ent_ch2_width_driver1.grid(row=10, column=14, sticky="w")
btn_ch2_width_driver1.grid(row=10, column=15, sticky="w")
status_ch2_width_driver1.grid(row=10, column=16, sticky="w")
## channel 3
lbl_ch3_driver1.grid(row=11, column=9, sticky="w")
switch_ch3_driver1.grid(row=11, column=10, sticky="w")
lbl_ch3_amp_driver1.grid(row=12, column=9, sticky="w")
ent_ch3_amp_driver1.grid(row=12, column=10, sticky="w")
btn_ch3_amp_driver1.grid(row=12, column=11, sticky="w")
status_ch3_amp_driver1.grid(row=12, column=12, sticky="w")
lbl_ch3_width_driver1.grid(row=12, column=13, sticky="w")
ent_ch3_width_driver1.grid(row=12, column=14, sticky="w")
btn_ch3_width_driver1.grid(row=12, column=15, sticky="w")
status_ch3_width_driver1.grid(row=12, column=16, sticky="w")
## channel 4
lbl_ch4_driver1.grid(row=13, column=9, sticky="w")
switch_ch4_driver1.grid(row=13, column=10, sticky="w")
lbl_ch4_amp_driver1.grid(row=14, column=9, sticky="w")
ent_ch4_amp_driver1.grid(row=14, column=10, sticky="w")
btn_ch4_amp_driver1.grid(row=14, column=11, sticky="w")
status_ch4_amp_driver1.grid(row=14, column=12, sticky="w")
lbl_ch4_width_driver1.grid(row=14, column=13, sticky="w")
ent_ch4_width_driver1.grid(row=14, column=14, sticky="w")
btn_ch4_width_driver1.grid(row=14, column=15, sticky="w")
status_ch4_width_driver1.grid(row=14, column=16, sticky="w")
# Attenuators
lbl_att.grid(row=15, column=3)
lbl_att1.grid(row=16, column=3)
btn_att1_10dB.grid(row=17, column=2, sticky="w")
btn_att1_20dB.grid(row=17, column=3, sticky="w")
btn_att1_40dB.grid(row=17, column=4, sticky="w")
lbl_att2.grid(row=18, column=3)
btn_att2_10dB.grid(row=19, column=2, sticky="w")
btn_att2_20dB.grid(row=19, column=3, sticky="w")
btn_att2_40dB.grid(row=19, column=4, sticky="w")
# selectors
motors_selector.grid(row=20, column=1, sticky="w")
attenuators_selector.grid(row=20, column=2, sticky="w")
LEDs_selector.grid(row=20, column=3, sticky="w")

# start the gui loop
window.mainloop()
