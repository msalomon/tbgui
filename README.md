# TBGUI description

Test Beam GUI for the LHCb ECAL Upgrade II activities. Currently, the devices implemented are:
Zaber motors for two axes rotation (azimuth and altitude)
LED drivers (2x, 4 channels each) for calibration purposes
Attenuators (2x) to study optimal configuration with a single cell, double readout.
Testing without devices should be done with all the following flags in the code set to zero (button implementation ongoing)
motors =      0       
LED =         0       
attenuators = 0      

# Prerequisites
## generic Drivers installation
sudo apt update
sudo apt upgrade
sudo apt install python3-pip
sudo pip3 install pyusb
## Zaber motors:
sudo pip3 install --user zaber-motion
## Python Visa libraries for BNC LED drivers SCPI coomunication
sudo pip3 install pyvisa
sudo pip3 install pyvisa-py
## LED drivers USB configuration
create the following rules for USB device permanent renaming, to be put in /etc/udev/rules.d/99-usb-serial.rules
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="AD0JJCVI", SYMLINK+="LEDdriver2"
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="AD0JJCVI", SYMLINK+="LEDdriver1"
to get information on the USB port: udevadm info --name=/dev/ttyUSB1 --attribute-walk
The rest is completely described in the code
## Attenuators USB configuration
arduino: create the following rules for USB device permanent renaming, to be put in /etc/udev/rules.d/99-usb-serial.rules
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{serial}=="557363133383513032C0", SYMLINK+="Attenuators"
Pymata for arduino Firmata communication:
sudo pip3 install pyserial (sudo -H pip3 install pyserial)
sudo pip3 install PyMata

# TODO list (known bugs)
- The checkbuttons for the device selection call the initialization both selecting and deselecting the check.
- Bind flags to value and not to the variable.
- The common OFF is not propagated in case of flag error.
- add a few slots for saving and recalling config.
- add log and recall the last saved slots from it.


